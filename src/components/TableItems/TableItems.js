import React from 'react';
import { Header, Table } from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';
import {connect} from 'react-redux';

export const TableItems = (props) =>{
  const { name, year, actors, _id } = props;
  const actorsFromState = props.items.entities.actors;
  return (
      <Table.Row >
        <Table.Cell onClick={() => props.history.push('/' + _id)}>
          <Header>
            {name}
          </Header>
        </Table.Cell>
        <Table.Cell>
          {year}
        </Table.Cell>
        <Table.Cell>
          {
            actors.map((actorId, i) => {return (
              <strong key={i}><b>Имена актеров: {actorsFromState[actorId].name}. </b>
                <b>Возраст: {actorsFromState[actorId].age}.</b>
              </strong>
            )})
          }
        </Table.Cell>
      </Table.Row>
  )
};

const mapStateToProps = ({films: { items }}) => ({
  items
});

// const app = withRouter(TableItems);
export default connect(mapStateToProps)(withRouter(TableItems))