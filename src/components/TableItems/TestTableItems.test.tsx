import React from 'react';
import {shallow} from 'enzyme';
import  { TableItems }  from './TableItems';
import Enzyme from "enzyme";
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({
  adapter: new Adapter()
});



describe('Test Table component', () => {

  const props:any = {
    actors: ['5d84a87f1350e7255838036c', '5d84a87f1350e7255838036c'],
    error: null,
    items: {
      entities: {
        actors: {
          '5d84a87f1350e7255838036c': {
            age: 30,
            name: "Александр Петров",
            __v: 0,
            _id: "5d84a87f1350e7255838036c"
          }
        },
        films: {
          '5d84aa5501f44e297807d08e': {
            name: "Полицейский с рублевки ",
            year: 201889,
            __v: 0,
            _id: "5d84aa5501f44e297807d08e",
            actors: ["5d84a87f1350e7255838036c"]
          }
        }
      },
      result: ["5d84aa5501f44e297807d08e", "5d84aa7601f44e297807d08f"]
    },
    loading: false
  };

  it('Test length items', () => {
     const wrapper = shallow(<TableItems {...props} />);
    expect(wrapper.find('strong')).toHaveLength(2);
   });
});