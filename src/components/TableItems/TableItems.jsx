"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var semantic_ui_react_1 = require("semantic-ui-react");
var react_router_dom_1 = require("react-router-dom");
var react_redux_1 = require("react-redux");
var TableItems = function (props) {
    var name = props.name, year = props.year, actors = props.actors, _id = props._id;
    var actorsFromState = props.items.entities.actors;
    return (<semantic_ui_react_1.Table.Row>
        <semantic_ui_react_1.Table.Cell onClick={function () { return props.history.push('/' + _id); }}>
          <semantic_ui_react_1.Header>
            {name}
          </semantic_ui_react_1.Header>
        </semantic_ui_react_1.Table.Cell>
        <semantic_ui_react_1.Table.Cell>
          {year}
        </semantic_ui_react_1.Table.Cell>
        <semantic_ui_react_1.Table.Cell>
          {actors.map(function (actorId, i) {
        return (<strong key={i}><b>Имена актеров: {actorsFromState[actorId].name}. </b>
                <b>Возраст: {actorsFromState[actorId].age}.</b>
              </strong>);
    })}
        </semantic_ui_react_1.Table.Cell>
      </semantic_ui_react_1.Table.Row>);
};
var mapStateToProps = function (_a) {
    var items = _a.films.items;
    return ({
        items: items
    });
};
var app = react_router_dom_1.withRouter(TableItems);
exports.default = react_redux_1.connect(mapStateToProps)(app);
