import * as React from "react";
import { Header, Table } from 'semantic-ui-react';
import {RouteComponentProps, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {FilmsToMap, IState, ownProps} from "../../types/typeAction";

export const TableItems: React.FunctionComponent<ownProps & RouteComponentProps> = (props: ownProps & RouteComponentProps) =>{

  const { name, year, actors, _id }:FilmsToMap = props;
  const actorsFromState = props.items.entities.actors;

  return (
      <Table.Row >
        <Table.Cell onClick={() => props.history.push('/' + _id)}>
          <Header>
            {name}
          </Header>
        </Table.Cell>
        <Table.Cell>
          {year}
        </Table.Cell>
        <Table.Cell>
          {
            Object.keys(actors).map((actorId: string, i: number) => {return (
              <strong key={i}><b>Имена актеров: {actorsFromState[actorId].name}. </b>
                <b>Возраст: {actorsFromState[actorId].age}.</b>
              </strong>
            )})
          }
        </Table.Cell>
      </Table.Row>
  )
};

const mapStateToProps = ({films: { items }}: IState) => ({
  items
});

export default connect(mapStateToProps)(withRouter(TableItems))