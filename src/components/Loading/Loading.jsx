"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var semantic_ui_react_1 = require("semantic-ui-react");
var LoaderExampleSizesInverted = function () { return (<div>
    <semantic_ui_react_1.Segment>
      <semantic_ui_react_1.Dimmer active inverted>
        <semantic_ui_react_1.Loader size='large'>Loading</semantic_ui_react_1.Loader>
      </semantic_ui_react_1.Dimmer>

      <semantic_ui_react_1.Image src='https://react.semantic-ui.com/images/wireframe/paragraph.png'/>
    </semantic_ui_react_1.Segment>
  </div>); };
exports.default = LoaderExampleSizesInverted;
