"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
require("./App.css");
var react_router_dom_1 = require("react-router-dom");
var Table_1 = require("./containers/Table/Table");
var FilmDetail_1 = require("./containers/FilmDetail/FilmDetail");
var App = function () {
    return (<react_router_dom_1.Switch>
      <react_router_dom_1.Route path={'/:_id'} component={FilmDetail_1.default}/>
      <react_router_dom_1.Route path={'/'} component={Table_1.default}/>
    </react_router_dom_1.Switch>);
};
exports.default = App;
