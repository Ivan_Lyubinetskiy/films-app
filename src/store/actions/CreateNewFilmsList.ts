import axios from "axios";
import {setFilmsError, setFilmsSuccess} from "./filmsActions";
import {Films, Data, IState} from "../../types/typeAction";
import {Dispatch} from "redux";
import {TypesAction} from "../redusers/filmsReducer";

export const setNewFilms = (film:Films) =>{
  return async (dispatch: Dispatch<TypesAction>, getState:() => IState) =>{
    try {
      await axios.post('/films/update', film);
      const store:Data = getState().films.items;
      dispatch(setFilmsSuccess(store));
    } catch (e) {
      dispatch(setFilmsError(e));
    }
  }
};



