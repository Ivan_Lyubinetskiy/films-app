import axios from 'axios'
import { normalize, schema } from 'normalizr'

const normalizeActors = new schema.Entity('actors', undefined,
    { idAttribute: value => value._id }
  );

const normalizeFilms = new schema.Entity(
    'films',
    { actors: [normalizeActors ]},
    { idAttribute: value => value._id }
  );

export const setFilms = () => {
  return async dispatch => {
    dispatch(setFilmsStart());
    try {
      const response = await axios.get('/films');
      const normalizedData = normalize(response.data, [normalizeFilms]);
      dispatch(setFilmsSuccess(normalizedData));
      // const store = getState().films.items;
      // setStoreToLocalStorage(store);
    } catch (e) {
      dispatch(setFilmsError(e))
    }
  }
};

export const setFilmsStart = () =>{
  return {
   type: 'SET_FILMS_START'
  }
};

export const setFilmsSuccess = (films) =>{
  return {
    type: 'SET_FILMS_SUCCESS',
    payload: films
  }
};

export const setFilmsError = (e) =>{
  return {
    type: 'SET_FILMS_ERROR',
    payload: e
  }
};

// const setStoreToLocalStorage = (state) =>{
//   localStorage.clear();
//   const films = JSON.stringify(state);
//   localStorage.setItem('state', films);
// };