import axios from "axios";
import {setFilmsError, setFilmsSuccess} from "./filmsActions";

export const setNewFilms = (film) =>{
  return async (dispatch, getState) =>{
    try {
      await axios.post('/films/update', film);
      const store = getState().films.items;
      dispatch(setFilmsSuccess(store));
    } catch (e) {
      dispatch(setFilmsError(e));
    }
  }
};



