import axios, {AxiosResponse} from 'axios';
import { normalize, schema } from 'normalizr';
import {Data, Film, State} from "../../types/typeAction";
import { action, ActionType } from "typesafe-actions";
import {ThunkAction} from "redux-thunk";

const normalizeActors = new schema.Entity('actors', undefined,
    { idAttribute: value => value._id }
  );

const normalizeFilms = new schema.Entity(
    'films',
    { actors: [normalizeActors ]},
    { idAttribute: value => value._id }
  );

export type setFilmType = ThunkAction<void, State, void, FilmAction>;

export const setFilms: setFilmType = (dispatch): void => {
      dispatch(setFilmsStart());
      axios.get<Film[]>('/films').then((response: AxiosResponse<Film[]>) => {
          const normalizedData:Data = normalize(response.data, [normalizeFilms]);
          dispatch(setFilmsSuccess(normalizedData));
      }).catch((error: Error) => {
          dispatch(setFilmsError(error));
      })
};

export const filmActions = {
  setFilmsStart: () => action('SET_FILMS_START'),
  setFilmsSuccess: (normalizedData:Data) => action('SET_FILMS_SUCCESS', normalizedData),
  setFilmsError: (e:Error) => action('SET_FILMS_ERROR', e),
};

export const { setFilmsStart, setFilmsSuccess, setFilmsError } = filmActions;

export type FilmAction = ActionType<typeof filmActions>;