import {setFilmsStart, setFilmsSuccess, setFilmsError} from "./filmsActions";
import * as actionsType from './filmsActions';

describe('Actions test', () => {
  it('Test action setFilmsStart', () => {
    const expectedAction: actionsType.FilmAction = {
      type:'SET_FILMS_START'
    };
    expect(setFilmsStart()).toEqual(expectedAction);
  });

  it('Test action setFilmsSuccess', () => {
    const expectedAction: actionsType.FilmAction = {
      type:'SET_FILMS_SUCCESS',
      payload: {
        entities: {
          films: {
          },
          actors: {  
          }
      },
      result: ['1','2']
      }
    };
    expect(setFilmsSuccess(expectedAction.payload)).toEqual(expectedAction)
  });

  it('Test action setFilmsError', () => {
    const expectedAction: actionsType.FilmAction = {
      type:'SET_FILMS_ERROR',
      payload: new Error('Some going wrone')
    };
    expect(setFilmsError(expectedAction.payload)).toEqual(expectedAction)
  });
});