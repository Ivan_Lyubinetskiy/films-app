const initialState = {
  loading: false,
  items: {
    entities: {
      films: {}
    }
  },
  error: null
};

export default (state = initialState, action) =>{
  switch (action.type) {
    case 'SET_FILMS_START':
      return{
        ...state,
        loading: true
      };
    case 'SET_FILMS_SUCCESS':
      return{
        ...state,
        loading: false,
        items: action.payload
      };
    case 'SET_FILMS_ERROR':
      return{
        ...state,
        loading: false,
        error: action.payload
      };
    // case 'CREATE_NEW_FILMS':
    //   return{
    //     ...state,
    //     loading: false,
    //     items: action.payload
    //   };
    default:
      return state;
  }
};

