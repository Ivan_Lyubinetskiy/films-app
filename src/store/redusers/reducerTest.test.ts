import reducer, { initialState } from './filmsReducer'
import * as actionsType from '../actions/filmsActions';

describe('Test reducer', () =>{

  it('Start loading without error', () => {

    const action:actionsType.FilmAction = {
      type: 'SET_FILMS_START'
    };

    expect(reducer(initialState, action)).toEqual({
      ...initialState,
      loading: true,
      error: null,
      items: {
        entities: {
          films: {}
        }
      }
    })
  });


  it('Loaded films with success', () => {

    const StateBefore = {
      loading: true,
      error: null,
      items: {
        entities: {
          films: {}
        }
      }
    };

    const action:actionsType.FilmAction = {
      type: 'SET_FILMS_SUCCESS',
      payload: {
        entities: {
          films: {
          },
          actors: {  
          }
      },
      result: ['1','2']
      }
    };

    expect(reducer(StateBefore, action)).toEqual({
      ...StateBefore,
      loading: false,
      items: action.payload
    })
  });

  it('Loaded films with errors', () => {

    const StateBefore = {
      loading: false,
      error: null,
      items: {
        entities: {
          films: {
          }
        }
      }
    };

    const action:actionsType.FilmAction = {
      type: 'SET_FILMS_ERROR',
      payload: new Error('Some going wrone')
    };

    expect(reducer(StateBefore, action)).toEqual({
      ...StateBefore,
      error: action.payload
    })
  })
});