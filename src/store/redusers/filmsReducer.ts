import * as actionsType from '../actions/filmsActions';
import {ActionType, getType} from 'typesafe-actions';
import { State } from '../../types/typeAction';

export type TypesAction = ActionType<typeof actionsType>;

export const initialState: State = {
  loading: false,
  items: {
    entities: {
      films: {}
    }
  },
  error: null
};

export default (state = initialState, action: actionsType.FilmAction): State => {
  switch (action.type) {
    case 'SET_FILMS_START':
      return{
        ...state,
        loading: true
      };
      case 'SET_FILMS_SUCCESS':
      return{
        ...state,
        loading: false,
        items: action.payload
      };
      case getType(actionsType.setFilmsError):
      return{
        ...state,
        loading: true,
        error: action.payload
      };
    default:
      return state;
  }
};
