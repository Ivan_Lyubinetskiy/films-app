import React from 'react'
import './App.css'
import { Switch, Route } from 'react-router-dom'
import Table from "./containers/Table/Table"
import FilmDetail from './containers/FilmDetail/FilmDetail'

const App = () => (
    <Switch>
      <Route path={'/:_id'} component={FilmDetail} />
      <Route path={'/'} component={Table} />
    </Switch>

);

export default App
