export interface Films {
    actors: string[],
    name: string,
    year: number
}
export interface FilmsToMap extends Films{
    _id: string
}

interface Actors {
    name: string,
    age: number
}

export interface Data {
    entities: {
        films: {
            [_id: string]: Films;
        }
        actors: {
            [_id: string]: Actors;
        }
    }
    result: Array<string>
}

export interface ActionStart {
    type: 'SET_FILMS_START',
}

export interface ActionSuccess {
    payload: Data,
    type: 'SET_FILMS_SUCCESS',
}
export interface ActionError {
    payload: string,
    type: 'SET_FILMS_ERROR'
}

export type AppActions = ActionStart | ActionSuccess | ActionError


export interface TableProps {
    loading: boolean,
    items: Data,
    error: Error,
    setFilms: any
}

export type Film = {
    name: string;
    year: number;
}
export type State = {
    loading: boolean,
    error: any,
    items: {
        entities: {
            films: Record<string, Film>;
        }
    }
}
export type StateFromProps = {
    error: string,
    items: Data,
    loading: boolean,
    setNewFilms: (arg:Films) => void
    match:{
        params:{
            _id: string
        }
    },
    history:{
        push: (arg:string) => void
    }
}

export interface IState {
    films: {
        items:{
            entities: {
                films: {
                    [_id: string]: Films;
                }
                actors: {
                    [_id: string]: Actors;
                }
            },
            result: Array<string>;
        },
        loading: boolean,
        error: any
    }
}

export interface ownProps {
    name: string,
    year: number,
    _id: string,
    items: {
        entities: {
            films: {
                [_id: string]: Films;
            }
            actors: {
                [_id: string]: Actors;
            }
        }
        result: Array<string>
    },
    actors: string[]
}

export interface PropsTest {
    actors: string[],
    loading: boolean,
    items: {
      entities: {
        films: object,
        actors: string[]
      },
      result?: number[]
    },
    setFilms: () => void,
    error?: Error
  }
 