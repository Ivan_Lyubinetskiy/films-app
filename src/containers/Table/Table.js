import React, { useEffect } from 'react'
import { Table, Container, Header } from 'semantic-ui-react'
import {connect} from 'react-redux'
import { setFilms } from '../../store/actions/filmsActions'
import TableItems from '../../components/TableItems/TableItems'
import Loading from "../../components/Loading/Loading"


export const TableExampleSelectableRow = (props) => {
  useEffect(() => {
    props.setFilms()
  },[]);
  const { items, loading, error } = props;
  const films = Object.values(items.entities.films);
  return (
    <Container>
      <Header as='h1'>Films Page</Header>
      {
        loading
          ? <Loading />
          : <Table celled selectable>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Films</Table.HeaderCell>
                <Table.HeaderCell>Date</Table.HeaderCell>
                <Table.HeaderCell>Actors</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            {error && <p>{error}</p>}
            {films.map((item, i) => (
            <Table.Body key={i}>
              <TableItems {...item} />
            </Table.Body>
            ))}
          </Table>
      }
    </Container>
  )
};

const mapStateToProps = ({films: { items, loading, error }}) => ({
  items,
  loading,
  error
});
const mapDispatchToProps = dispatch => ({
  setFilms:() => dispatch(setFilms())
});
export default connect(mapStateToProps, mapDispatchToProps)(TableExampleSelectableRow)

