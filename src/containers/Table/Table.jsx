"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = require("react");
var semantic_ui_react_1 = require("semantic-ui-react");
var react_redux_1 = require("react-redux");
var filmsActions_1 = require("../../store/actions/filmsActions");
var TableItems_1 = require("../../components/TableItems/TableItems");
var Loading_1 = require("../../components/Loading/Loading");
var TableExampleSelectableRow = function (props) {
    react_1.useEffect(function () {
        props.setFilms();
    }, []);
    var items = props.items, loading = props.loading;
    var films = Object.values(items.entities.films);
    return (<semantic_ui_react_1.Container>
      <semantic_ui_react_1.Header as='h1'>Films Page</semantic_ui_react_1.Header>
      {loading
        ? <Loading_1.default />
        : <semantic_ui_react_1.Table celled selectable>
            <semantic_ui_react_1.Table.Header>
              <semantic_ui_react_1.Table.Row>
                <semantic_ui_react_1.Table.HeaderCell>Films</semantic_ui_react_1.Table.HeaderCell>
                <semantic_ui_react_1.Table.HeaderCell>Date</semantic_ui_react_1.Table.HeaderCell>
                <semantic_ui_react_1.Table.HeaderCell>Actors</semantic_ui_react_1.Table.HeaderCell>
              </semantic_ui_react_1.Table.Row>
            </semantic_ui_react_1.Table.Header>
            {films.map(function (item, i) { return (<semantic_ui_react_1.Table.Body key={i}>
              <TableItems_1.default {...item}/>
            </semantic_ui_react_1.Table.Body>); })}
          </semantic_ui_react_1.Table>}
    </semantic_ui_react_1.Container>);
};
var mapStateToProps = function (_a) {
    var _b = _a.films, items = _b.items, loading = _b.loading;
    return ({
        items: items,
        loading: loading
    });
};
var mapDispatchToProps = function (dispatch) { return ({
    setFilms: function () { return dispatch(filmsActions_1.setFilms()); }
}); };
exports.default = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(TableExampleSelectableRow);
