import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from "redux";
import { Container, Header, Table } from 'semantic-ui-react';
import Loading from "../../components/Loading/Loading";
import TableItems from '../../components/TableItems/TableItems';
import { setFilms } from '../../store/actions/filmsActions';
import { AppActions, IState, TableProps } from "../../types/typeAction";


export const TableExampleSelectableRow: React.FC<TableProps> = props => {
  useEffect(() => {
    props.setFilms();
  },[]);
  const { items, loading, error }:TableProps = props;
  const films = Object.values(items.entities.films);

  return (
    <Container data-test="TableTest">
      <Header as='h1'>Films Page</Header>
      {
        loading
          ? <Loading />
          : <Table celled selectable>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Films</Table.HeaderCell>
                <Table.HeaderCell>Date</Table.HeaderCell>
                <Table.HeaderCell>Actors</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            {error && <p>{error}</p>}
            {films.map((item:any, i:number) => (
            <Table.Body key={i}>
              <TableItems {...item} />
            </Table.Body>
            ))}
          </Table>
      }
    </Container>
  )
};

const mapStateToProps = ({films: { items, loading, error }}:IState) => ({
  items,
  loading,
  error
});
const mapDispatchToProps = (dispatch:Dispatch<AppActions>) => bindActionCreators({
    setFilms: setFilms,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TableExampleSelectableRow)

