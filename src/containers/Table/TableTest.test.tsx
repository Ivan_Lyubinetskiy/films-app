import React from 'react';
import {shallow, mount} from 'enzyme';
import  { TableExampleSelectableRow }  from './Table';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { PropsTest } from '../../types/typeAction';

Enzyme.configure({
  adapter: new Adapter()
});


describe('Test Table component', () => {

  const props:PropsTest  = {
    actors: ["5d84a87f1350e7255838036c"],
    loading: false,
    items: {
      entities: {
        films: {},
        actors: ['3242','2342']
      }
    },
    setFilms: () => {}
  };
  describe('Test Table is loading', () => {

    it('Loading', () => {

      const nextProps:any = {
        ...props,
        loading: true
      };

      const wrapper = shallow(<TableExampleSelectableRow {...nextProps} />);
      expect(wrapper.find('LoaderExampleSizesInverted')).toHaveLength(1);
    })

  });

  describe('Test Table is loaded', () => {

    it('Loaded', () => {
      const nextProps:any = {
        ...props,
        items: {
          entities: {
            films: {},
            actors: ['3242','2342']
          },
          result: [1,2,3]
        },
        loading: false
      };
      const wrapper = shallow(<TableExampleSelectableRow {...nextProps} />);
      expect(wrapper.find('Table')).toHaveLength(1);
    })

  });

  describe('Test Table is loaded with error', () => {

    it('loaded with error', () => {
      const nextProps:any = {
        ...props,
        error: 'Some errors'
      };
      const wrapper = shallow(<TableExampleSelectableRow {...nextProps} />);
      expect(wrapper.find('p').text()).toEqual(nextProps.error);
    })

  });

  describe('Table install', () => {
    const mockFetchItem = jest.fn();
    const nextProps:any = {
      ...props,
      loading: true,
      setFilms: mockFetchItem
    };

    const wrapper = mount(<TableExampleSelectableRow { ...nextProps } />);

    it('Test Table isLoading', () => {
      expect(nextProps.setFilms).toHaveBeenCalled()
    })
  })
});




