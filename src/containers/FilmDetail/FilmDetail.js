import React from 'react';
import { Button, Card, Container, TextArea, Header } from 'semantic-ui-react';
import {connect} from 'react-redux';
import {setNewFilms} from "../../store/actions/CreateNewFilmsList";

export const FilmDetail = props =>{
// если получать все из storage - просто все раскоментировать, закоментировать c 8, 10, 11
  const { items } = props;
  const id = props.match.params._id;
  const film = items.entities.films[id];
  const actors = items.entities.actors;
// const getStateFromStorage = () => {
//   const stateFromStorage = localStorage.getItem('state');
//   const state = JSON.parse(stateFromStorage);
//   return state.entities
// };
//
// const { films, actors } = getStateFromStorage();
// const film = films[id];
  const goToHomePage = () => {
    props.history.push('/')
  };

  const onSaveChanges = () => {
    props.setNewFilms(film);
    goToHomePage()
  };

  const changeHandler = (e) =>{
    if(e.target.name === 'name'){
      film.name = e.target.value
    } else if (e.target.name === 'year') {
      film.year = e.target.value
    }
  };
   return (
    <Container
      style={{
        display: "flex",
        paddingTop: "100px",
        justifyContent: "center"
      }}>
      <Card>
        <Header as='h1'>Film card</Header>
        <Card.Content>
          <Card.Header>Film name :</Card.Header>
          <TextArea
            rows={1}
            name={'name'}
            onChange={changeHandler}
            defaultValue={film.name}
          >
              </TextArea>
          <Card.Meta>Film year :</Card.Meta>
          <TextArea
            rows={1}
            name={'year'}
            onChange={changeHandler}
            defaultValue={film.year}
          >
            </TextArea>
          <Card.Description>
            <strong>{film.actors.map(actorId => actors[actorId].name)}</strong>
          </Card.Description>
        </Card.Content>
        <Card.Content extra>
          <Button.Group>
            <Button onClick={goToHomePage}>Cancel</Button>
            <Button.Or />
            <Button data-test={'btn'} positive onClick={onSaveChanges}>Save</Button>
          </Button.Group>
        </Card.Content>
      </Card>
    </Container>
   )
};

const mapStateToProps = ({films: { items }}) => ({
  items
});
const mapDispatchToProps = dispatch => ({
  setNewFilms: films => dispatch(setNewFilms(films))
});
export default connect(mapStateToProps, mapDispatchToProps)(FilmDetail)