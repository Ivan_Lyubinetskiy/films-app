import * as React from "react";
import { Button, Card, Container, Header, TextArea } from 'semantic-ui-react';
import {connect} from 'react-redux';
import {setNewFilms} from "../../store/actions/CreateNewFilmsList";
import {AppActions, IState, StateFromProps} from "../../types/typeAction";
import {bindActionCreators, Dispatch} from "redux";


export const FilmDetail: React.FC<StateFromProps> = props => {
  const { items } = props;
  const id = props.match.params._id;
  const film = items.entities.films[id];
  const actors = items.entities.actors;

// const getStateFromStorage = () => {
//   const stateFromStorage = localStorage.getItem('state');
//   const state = JSON.parse(stateFromStorage);
//   return state.entities
// };
//
// const { films, actors } = getStateFromStorage();
// const film = films[id];

  const goToHomePage = ():void => {
    props.history.push('/')
  };

  const onSaveChanges = ():void => {
    props.setNewFilms(film);
    goToHomePage()
  };

  const changeHandler: React.ReactEventHandler<HTMLTextAreaElement> = (e: React.ChangeEvent<HTMLTextAreaElement>) =>{
    if(e.target.name === 'name'){
      film.name = e.target.value
    } else if (e.target.name === 'year') {
      film.year = +e.target.value
    }
  };
  Object.keys(actors)
   return (
    <Container
      style={{
        display: "flex",
        paddingTop: "100px",
        justifyContent: "center"
      }}>
      <Card>
        <Header as='h1'>Film card</Header>
        <Card.Content>
          <Card.Header>Film name :</Card.Header>
          <TextArea
            rows={1}
            name={'name'}
            onChange={changeHandler}
            defaultValue={film.name}
          >
              </TextArea>
          <Card.Meta>Film year :</Card.Meta>
          <TextArea
            rows={1}
            name={'year'}
            onChange={changeHandler}
            defaultValue={film.year}
          >
          </TextArea>
          <Card.Description>
            <strong>{film.actors.map((actorId:string) => actors[actorId].name)}</strong>
          </Card.Description>
        </Card.Content>
        <Card.Content extra>
          <Button.Group>
            <Button onClick={goToHomePage}>Cancel</Button>
            <Button.Or />
            <Button data-test={'btn'} positive onClick={onSaveChanges}>Save</Button>
          </Button.Group>
        </Card.Content>
      </Card>
    </Container>
   )
};

const mapStateToProps = ({films: { items }}:IState) => ({
  items
});
const mapDispatchToProps = (dispatch:Dispatch<AppActions>) => bindActionCreators({
    setNewFilms,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FilmDetail)