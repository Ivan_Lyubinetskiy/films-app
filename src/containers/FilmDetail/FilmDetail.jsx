"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var semantic_ui_react_1 = require("semantic-ui-react");
var react_redux_1 = require("react-redux");
var CreateNewFilmsList_1 = require("../../store/actions/CreateNewFilmsList");
var FilmDetail = function (props) {
    //TODO если получать все из storage - просто все раскоментировать, закоментировать c 8, 10, 11
    var items = props.items;
    var id = props.match.params._id;
    var film = items.entities.films[id];
    var actors = items.entities.actors;
    // const getStateFromStorage = () => {
    //   const stateFromStorage = localStorage.getItem('state');
    //   const state = JSON.parse(stateFromStorage);
    //   return state.entities
    // };
    //
    // const { films, actors } = getStateFromStorage();
    // const film = films[id];
    var goToHomePage = function () {
        props.history.push('/');
    };
    var onSaveChanges = function () {
        props.setNewFilms(film);
        goToHomePage();
    };
    var changeHandler = function (e) {
        if (e.target.name === 'name') {
            film.name = e.target.value;
        }
        else if (e.target.name === 'year') {
            film.year = e.target.value;
        }
    };
    return (<semantic_ui_react_1.Container style={{
        display: "flex",
        paddingTop: "100px",
        justifyContent: "center"
    }}>
      <semantic_ui_react_1.Card>
        <semantic_ui_react_1.Header as='h1'>Film card</semantic_ui_react_1.Header>
        <semantic_ui_react_1.Card.Content>
          <semantic_ui_react_1.Card.Header>Film name :</semantic_ui_react_1.Card.Header>
          <semantic_ui_react_1.TextArea rows={1} name={'name'} onChange={changeHandler} defaultValue={film.name}>
              </semantic_ui_react_1.TextArea>
          <semantic_ui_react_1.Card.Meta>Film year :</semantic_ui_react_1.Card.Meta>
          <semantic_ui_react_1.TextArea rows={1} name={'year'} onChange={changeHandler} defaultValue={film.year}>
          </semantic_ui_react_1.TextArea>
          <semantic_ui_react_1.Card.Description>
            <strong>{film.actors.map(function (actorId) { return actors[actorId].name; })}</strong>
          </semantic_ui_react_1.Card.Description>
        </semantic_ui_react_1.Card.Content>
        <semantic_ui_react_1.Card.Content extra>
          <semantic_ui_react_1.Button.Group>
            <semantic_ui_react_1.Button onClick={goToHomePage}>Cancel</semantic_ui_react_1.Button>
            <semantic_ui_react_1.Button.Or />
            <semantic_ui_react_1.Button positive onClick={onSaveChanges}>Save</semantic_ui_react_1.Button>
          </semantic_ui_react_1.Button.Group>
        </semantic_ui_react_1.Card.Content>
      </semantic_ui_react_1.Card>
    </semantic_ui_react_1.Container>);
};
var mapStateToProps = function (_a) {
    var items = _a.films.items;
    return ({
        items: items
    });
};
var mapDispatchToProps = function (dispatch) { return ({
    setNewFilms: function (films) { return dispatch(CreateNewFilmsList_1.setNewFilms(films)); }
}); };
exports.default = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(FilmDetail);
