import React from 'react';
import {shallow} from 'enzyme';
import  { FilmDetail }  from './FilmDetail';
import Enzyme from "enzyme";
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({
  adapter: new Adapter()
});

// interface Props {
//   setNewFilms: () => void,  
//   history: {
//     push: () => void
//   },
//   match: {
//     params: {
//       _id: string
//     }
//   },
//   items: {
//     entities: {
//       actors: {
//         [_id: string]:{
//           __v: number,
//           _id: string,
//           age: number,
//           name: string
//         }
//       },
//       films: {
//         [_id: string]:{
//           __v: number,
//           _id: string,
//           year: number,
//           name: string,
//           actors: string[]
//         }
//       }
//     },
//     result: string[]
//   },
//   film: {
//     __v: number,
//     _id: string,
//     year: number,
//     name: string,
//      actors: string[]
//   }
// }

describe('Test detail items', () => {

  const mockSetFilm = jest.fn();

  const props: any = {
    setNewFilms: mockSetFilm,
    history: {
      push: () => {}
    },
    match: {
      params: {
        _id: '5d84aa5501f44e297807d08e'
      }
    },
    film: {
        name: "Полицейский с рублевки ",
        year: 201889,
        __v: 0,
        _id: "5d84aa5501f44e297807d08e",
        actors: ["5d84a87f1350e7255838036c"]
      },
    items: {
      entities: {
        actors: {
          '5d84a87f1350e7255838036c': {
            age: 30,
            name: "Александр Петров",
            __v: 0,
            _id: "5d84a87f1350e7255838036c"
          }
        },
        films: {
          '5d84aa5501f44e297807d08e': {
            name: "Полицейский с рублевки ",
            year: 201889,
            __v: 0,
            _id: "5d84aa5501f44e297807d08e",
            actors: ["5d84a87f1350e7255838036c"]
          }
        }
      },
      result: ["5d84aa5501f44e297807d08e", "5d84aa7601f44e297807d08f"]
    }
  };

  describe('Click handler', () => {  
    const wrapper = shallow(<FilmDetail {...props} />)
    wrapper.find('[data-test="btn"]').simulate('click', {
      preventDefault: () => {}
    })

    it('calls setNewFilms', () => {
      expect(props.setNewFilms).toHaveBeenCalled()
    });
  })
})