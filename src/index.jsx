"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
require("./index.css");
var App_1 = require("./App");
var redux_1 = require("redux");
var react_redux_1 = require("react-redux");
var rootReducer_1 = require("./store/redusers/rootReducer");
var redux_thunk_1 = require("redux-thunk");
require("semantic-ui-css/semantic.min.css");
var react_router_dom_1 = require("react-router-dom");
var serviceWorker = require("./serviceWorker");
var composeEnhancers = typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : redux_1.compose;
var store = redux_1.createStore(rootReducer_1.default, composeEnhancers(redux_1.applyMiddleware(redux_thunk_1.default)));
var app = (<react_redux_1.Provider store={store}>
    <react_router_dom_1.BrowserRouter>
      <App_1.default />
    </react_router_dom_1.BrowserRouter>
  </react_redux_1.Provider>);
ReactDOM.render(app, document.getElementById('root'));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
